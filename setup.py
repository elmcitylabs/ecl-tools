#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2012-2019 Elm City Labs LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import runpy

try:
    from setuptools import setup  # type: ignore
except ImportError:
    from distutils.core import setup

metadata_filename = "ecl_tools/metadata.py"
metadata = runpy.run_path(metadata_filename)

setup(
    name="ecl_tools",
    version=metadata["__version__"],
    license=metadata["__license__"],
    url="https://bitbucket.org/elmcitylabs/ecl-tools/src/master/",
    description="Some useful utilities for Python",
    author="Dan Loewenherz",
    author_email="dan@elmcitylabs.com",
    packages=["ecl_tools"],
    install_requires=["redis>=3.3.11", "jsmin>=2.2.2", "slimit>=0.8.1"],
)

